if [ "$1" == "" ]; then 
    echo 'specify case number'
    exit 1
fi

case=$1

cd /tmp/ap/$case
rm -f makeStereo.pre.mp4 makeStereo.post.mp4
ffmpeg -i makeStereo.pre.wav -acodec libmp3lame makeStereo.pre.mp4
ffmpeg -i makeStereo.post.wav -acodec libmp3lame makeStereo.post.mp4

rm -f makeStereo.final.mp4 
#ffmpeg -framerate 30 -i makeStereo_%05d.png -c:v  libx264 -profile:v high -crf 20 -pix_fmt yuv420p makeStereo.1.mp4
ffmpeg -i makeStereo.mp4 -i makeStereo.pre.mp4 -acodec libmp3lame makeStereo.final.mp4
cp makeStereo.final.mp4 /home/jbf/project/uigit/rbsp/u/kris/2019/20190606/makeStereo.$case.mp4
