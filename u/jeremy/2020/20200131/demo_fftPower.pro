; return the frequency tags in KHz
function getFrequencyDomainTagsForPower, offsets, units, tounits
   n= n_elements(offsets)
   T= ( offsets[n-1]-offsets[0] ) / ( n-1 )
   Tcheck= offsets[1]-offsets[0]
   if ( abs( ( T-Tcheck ) / ( T ) ) gt 0.001 ) then begin
       stop, "timetags do not appear to be uniform"
   endif
   if ( units ne 'ns' ) then begin
       stop, "time offsets must be in ns"
   endif
   result= fltarr(n/2-1)
   if ( tounits eq 'KHz' ) then begin
      m= 1d6
   endif else if ( tounits eq 'Hz' ) then begin
      m= 1d9
   endif else begin
      stop, 'tounits must be KHz or Hz"
   endelse
   
   for i=0,n/2-2 do begin
      result[i]= (i+1) / ( n*T/m )
   endfor

   return, result
end

; fftPower demo 
pro demo_fftPower
  cdf= CDF_OPEN('/home/jbf/project/autoplot/data.nobackup/rbsp-a_HFR-waveform_emfisis-L1_20121112_v1.2.1.cdf')
  
  nrec= 1320  ; determined with cdfedit unix command
  
  CDF_VARGET, cdf, 'HFRsamples', buSamples, rec_count= nrec
  CDF_VARGET, cdf, 'Epoch', epoch, rec_count=nrec
  CDF_VARGET, cdf, 'timeOffsets', timeOffsets, rec_count=1
  timeOffsets= double( reform( timeOffsets ) )
    
  CDF_VARGET, cdf, 'fftSize', fftSize, rec_count=1
  fftSize= 256
  
  powTags= getFrequencyDomainTagsForPower(timeOffsets[0:fftSize-1],'ns','Hz');
  
  lenwave= n_elements( timeOffsets )
  
  ; number of result records
  nt= nrec*(lenwave/fftSize)
  
  result= fltarr( nt, n_elements(powTags) )
  irec=0
  tt= dblarr( nt )
  
  binsizeHz= ( powTags[2] - powTags[1] )
  
  ;window= hanning(fftSize,/double)
  window= replicate(1,fftSize)
  
  for i=0,nrec-1 do begin
     wave= buSamples[*,i]
     for j=0,lenwave/fftSize-1 do begin
        wav= wave[ j*fftSize:((j+1)*fftSize-1) ]
        ;dc= mean(wav,/double)
        ;wav= wav-dc
        fft1= 2 * abs( fft( window * wav ) ) ^ 2 / binsizeHz 
        result[irec,*]= fft1[1:fftSize/2-1]
        tt[irec]= epoch[i] + timeOffsets[j*fftSize]
        irec= irec+1
        dt= powTags[1]-powTags[0]  
     endfor
  endfor
  
  help, tt, powTags, result
  
  qds=0
  if ( qds ) then begin
     applot, tt, powTags/1000, result,  xunits='cdfTT2000', ytitle='Spec x', yunits='Hz', /noplot
     ;applot, 1, tt, powTags/1000, result,  xunits='cdfTT2000', ytitle='Spec x', yunits='Hz'
     ;applot, 3, tt, tt, xunits='cdfTT2000'
  endif else begin
     file_delete, 'foo.cdf', /ALLOW_NONEXISTENT
     cdf= cdf_create( 'foo.cdf' )
     v= cdf_varcreate( cdf, 'tt', /zvar )
     cdf_varput, cdf, v, tt, /zvar
     v= cdf_varcreate( cdf, 'result', [ 1 ], dim=[127], /zvar, /rec_vary )
     for i=0,n_elements(tt)-1 do begin
        cdf_varput, cdf, v, reform(result[i,*]), /zvar, rec_start=i
     endfor
     v= cdf_varcreate( cdf, 'ytags', [ 127 ], /zvar )
     cdf_varput, cdf, v, powTags/1000, /zvar
  
     a= cdf_attcreate( cdf, 'DEPEND_0', /VARIABLE )
     a= cdf_attcreate( cdf, 'DEPEND_1', /VARIABLE )
     cdf_attput, cdf, 'DEPEND_0', 'result', 'tt'
     cdf_attput, cdf, 'DEPEND_1', 'result', 'ytags'
  
     cdf_close, cdf
  
  endelse

end 
