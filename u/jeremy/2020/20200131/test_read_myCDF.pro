; test read_myCDF.pro
; wget https://cdaweb.gsfc.nasa.gov/data/rbsp/rbspa/l2/emfisis/wfr/waveform/2019/rbsp-a_wfr-waveform_emfisis-l2_20190101_v1.6.2.cdf

;.comp applot
;.comp demo_fftPower.pro
;.comp spdf_read_utilities.pro
;.comp virtual_funcs.pro
;.comp read_myCDF.pro

data= read_myCDF( 'BuSamples', 'rbsp-a_wfr-waveform_emfisis-l2_20190101_v1.6.2.cdf' )
x= fftPower( data, index=0 )
applot, *x[0], *x[1], *x[2]